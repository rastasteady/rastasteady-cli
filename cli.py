#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import lib.rastasteady as RastaSteady

@click.command(no_args_is_help=True, context_settings=dict(max_content_width=120))

@click.argument('video', type=str, required=True)

@click.option('--no-estabiliza', type=bool, is_flag=True, default=False, help='Desactiva la estabilización del video.', show_default=True)
@click.option('--dual', type=bool, is_flag=True, default=False, help='Crea fichero dual con video original y procesado.', show_default=True)
@click.option('--rastaview', type=int, default=20, help='Porcentaje de efecto Rastaview a aplicar. Usar 0 para desactivar el efecto.', show_default=True)

@click.version_option('0.1')

def cli(video, no_estabiliza, rastaview, dual):
    """RastaSteady es un software de estabilizacion de video para el sistema DJI FPV digital."""
    click.echo('Procesando %s!' % video)

    myVideo = RastaSteady.RastaSteady(video)
    if not no_estabiliza:
        myVideo.stabilize()
        if rastaview > 0:
            myVideo.rastaview(percentage = rastaview)
    if myVideo.dual():
        myVideo.dual()
